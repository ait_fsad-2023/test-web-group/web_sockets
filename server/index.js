const http = require('http').createServer();
const WebSocket = require('ws')

const io = require('socket.io')(http, {
    cors: {origin: "*"}
})

const server = new WebSocket.Server({port: '8000'})

server.on('connection', socket => {
    socket.on('message', message => {
        socket.send(`Roger that! ${message}`);
    });
});

io.on('connection', socket => {
    socket.on('message', (message) => {
        console.log(message);
        io.emit('message', `${socket.id.substr(0,2)} said ${message}`);
    });
});

http.listen(3000, () => {
    console.log('Socket IO listening on port 3000')
});